# fva

Funding Valuation Adjustment (FVA) is introduced to capture the incremental costs of funding uncollateralized derivatives. It can be referred to as the difference between the rate paid for the collateral to the bank’s treasury and rate paid by the cl